const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');



module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },


  module:{
    rules:[
      {
        test:/.css$/,
        use: ['style-loader', 'css-loader']
        // use: ExtractTextPlugin.extract({
        //       fallback: "style-loader",
        //       use: ["css-loader"],
        // })
      },
      {
        test: /\.js$|jsx$/,
        exclude: /node_modules/,
        use: "babel-loader"
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader?name=[name].[ext]&publicPath=&outputPath=images/',
          'img-loader'
         ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
         use: [
           'file-loader?name=[name].[ext]&publicPath=&outputPath=fonts/'
         ]
       }
    ]
  },

  devServer: {
    compress: true,
    noInfo: true,
    overlay: true,
    hot:true,
    inline: true,
  },

  plugins: [
        new ExtractTextPlugin({
          filename: "style.css",
          disable: true,
          allChunks: true
        }),

        new HtmlWebpackPlugin({
          title: 'Enstamasonry',
          template: './src/index.html',
          inject:'body',
          favicon:'./src/public/images/landing_page_photos/instagram1.png'
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ]

};
