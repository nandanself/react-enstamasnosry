import React from "react";
import ScaledImage from "./scaled-image";

export const TopNavbar = ( ) => {
  return (
    <nav>
      <title>Enstamasonry</title>
      <input type="text" placeholder="search"/>
      <ScaledImage src="https://d2r0rrogy5uf19.cloudfront.net/userDPs/c5468197-c423-3d3a-a79a-a1f9ab8cf8d6.jpg"/>
    </nav>
  );
};
