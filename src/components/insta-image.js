import React from "react";
import { LoaderSpinner } from "./loader-spinner";

export const InstaImage = props => {

  const st = {
    height: props.computedHeight
  };

  if (props.inViewPort) {
    return (
      <div style={st}>
        <a download={props.image} href={props.image}>
          <img
            className="downloadIcon"
            src={require("../public/images/insta_card_icons/download.png")}
          />
        </a>
        <img className="insta-image image-animation"
          src={props.image}
        />
      </div>
    );
  } else {
    return (
      <div style={st}>
        <LoaderSpinner />
      </div>
    );
  }
};
