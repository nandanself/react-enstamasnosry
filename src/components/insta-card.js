import React, { Component } from "react";
import ReactDOM from 'react-dom'
import { InstaImage } from "./insta-image";
import { ViewPort } from "../mixins/viewport";
import ScaledImage from './scaled-image';

export default class InstaCard extends Component {

  constructor( props ) {
    super( props );
    this.state = {
      image: props.data,
      inViewPort: false
    };
  }

  handleScroll( ) {
    var currentState = this;
    let node = ReactDOM.findDOMNode( this );
    setTimeout( function( ) {
      if (ViewPort( node )) {
        currentState.setState({ inViewPort: true });
        node.classList.add( 'card-animation' );
      }
    }.bind( this ), 0);
  }

  componentDidMount( ) {
    this.handleScroll( );
    window.addEventListener('scroll', this.handleScroll.bind( this ));
  }

  componentWillUnmount( ) {
    window.removeEventListener( 'scroll', this.handleScroll );
  }

  photoLikedAndUnLiked( ) {
    let image = Object.assign( {}, this.state.image );
    image.isLiked = !image.isLiked;
    this.setState({ image });
  }

  render( ) {
    let style = {
      position: "absolute"
    };
    let image = this.state.image;
    let heart = null;
    if ( image.isLiked ) {
      heart = <img onClick={this.photoLikedAndUnLiked.bind( this )} className="heart-icon" src={require( "../public/images/insta_card_icons/heart_red.png" )}/>;
    } else {
      heart = <img onClick={this.photoLikedAndUnLiked.bind( this )} className="heart-icon" src={require( "../public/images/insta_card_icons/heart_white.png" )}/>;
    }
    return (
      <article style={style} className="insta-card">
        <div className="headerBox">
          <ScaledImage src={image.profilePic}/>
          <div className="username">
            <a href="">
              {image.name}
            </a>
            <a href="">
              {image.location}
            </a>
          </div>
        </div>
        <InstaImage image={image.image} computedHeight={image.computedHeight} inViewPort={this.state.inViewPort}/>
        <div className="ember-view card-footer">
          {heart}
        </div>
      </article>
    );
  };
};
