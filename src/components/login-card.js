import React, { Component } from "react";
import ScaledImage from "./scaled-image";

export default class LoginCard extends Component {
  render( ) {
    return (
      <div className="login-card">
        <title>Enstamasonry</title>
        <div className="insta-card ">
          <div className="headerBox">
            <ScaledImage src="https://instagram.fblr2-1.fna.fbcdn.net/t51.2885-19/s150x150/14719833_310540259320655_1605122788543168512_a.jpg"/>
            <div className="username">
              <a href="#">Instagram</a>
            </div>
          </div>
          <div className="center-image">
            <ScaledImage src={require( "../public/images/landing_page_photos/instagram-card.jpg" )}/>
          </div>
          <div className="card-footer">
            <img className="heart-icon" src={require( "../public/images/insta_card_icons/heart_red.png" )}/>
          </div>
        </div>
        <div className="login-strip">
          <img src={require( "../public/images/landing_page_photos/instagram1.png" )}/>
          <p>Continue with Instagram</p>
        </div>
      </div>
    );
  };
}
