import React from "react"

export const SlidingPage = ( props ) => {
  let famousPerson = props.photos.map(( data, i ) => {
    return (
      <div className="landing-card" key={i}>
        <div className="profile-pic">
          <img src={require( "../public/images/landing_page_photos/taylor_swift.jpg" )}/>
        </div>
        <p>Taylor Swift</p>
      </div>
    )
  });

  return (
    <div className="sliding-page">
      <div className="cover"></div>
      {famousPerson}
    </div>
  );
};
