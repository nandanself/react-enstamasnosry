import React from "react";
import ScaledImage from "./scaled-image";

export const UserDetail = ( props ) => {
  return (
    <header className="user-detailBox">
      <div className="user">
        <ScaledImage src="https://d2r0rrogy5uf19.cloudfront.net/userDPs/c5468197-c423-3d3a-a79a-a1f9ab8cf8d6.jpg"/>
        <div className="details">
          <div className="first-strip">
            <h2>nandanself</h2>
          </div>

          <div className="second-strip">
            <div className="col">
              <p>6 posts</p>
            </div>

            <div className="col">
              <p>106 followers</p>
            </div>

            <div className="col">
              <p>164 following</p>
            </div>
          </div>

          <div className="third-strip">
            <p>Maruti Nandan</p>
            <span>Enstamasonry</span>
            <a href="https:galleri5.com">m.youtube.com/watch?feature=youtu.be&v=wo0ospGvxXc</a>
          </div>
        </div>
      </div>
    </header>
  )
}
