import React from 'react';
import ReactDOM from 'react-dom';
import App from "./container/App"
import css from "./app.css";

ReactDOM.render(
  <App/>, document.getElementById( 'root' ));
