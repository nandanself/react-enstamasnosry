import React, { Component } from "react";
import { DummyData } from "../dummy_data";
import InstaCard from "../components/insta-card";

export default class GridLayout extends Component {
  componentDidMount( ) {
    // console.log(DummyData)
    let pad = 10,
      newleft,
      newtop;
    var photosCard = document.getElementsByClassName( "insta-card" );
    for ( var j = 1; j < photosCard.length; j++ ) {
      photosCard[j].style.top = "0px";
      photosCard[j].style.left = "0px";
    }

    const cardSize = 300;
    let documentWidth = document.documentElement.clientWidth - 40;
    var numberOfColumn = 0;
    let numCards = Math.floor( documentWidth / cardSize );
    if ( numCards * cardSize + ( numCards - 1 ) * pad < documentWidth ) {
      numberOfColumn = Math.floor( documentWidth / cardSize );
    } else if ( numCards * cardSize + ( numCards - 1 ) * pad > documentWidth ) {
      numberOfColumn = Math.floor( documentWidth / cardSize ) - 1;
    }
    let gridHeight = 0;
    let cols = numberOfColumn;
    for ( var i = 1; i < photosCard.length; i++ ) {
      if ( i % cols === 0 ) {
        newtop = photosCard[i - cols].offsetTop + photosCard[i - cols].offsetHeight + pad;
        photosCard[i].style.top = newtop + "px";

        let height = newtop + photosCard[i].offsetHeight;
        if ( gridHeight < height ) {
          gridHeight = height;
        }
      } else {
        if (photosCard[i - cols]) {
          newtop = photosCard[i - cols].offsetTop + photosCard[i - cols].offsetHeight + pad;
          photosCard[i].style.top = newtop + "px";
          let height = newtop + photosCard[i].offsetHeight;
          if ( gridHeight < height ) {
            gridHeight = height;
          }
        }
        newleft = photosCard[i - 1].offsetLeft + photosCard[i - 1].offsetWidth + pad;
        photosCard[i].style.left = newleft + "px";
        // console.log("else",i);
      }
    }
    gridHeight = "height:" + gridHeight + "px";
    // this.set('gridHeight',gridHeight);
  }

  render( ) {
    return (
      <div className="grid-layout" style={{
        width: "1230px",
        height: "1940px"
      }}>
        {DummyData.map(( data, i ) => {
          data.computedHeight = 300 * data.height / data.width + "px";
          return <InstaCard key={i} data={data}/>;
        })}
      </div>
    );
  }
}
