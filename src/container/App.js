import React, { Component } from "react";
import UserPage from "./user-page";
import LandingPage from "./landing-page";

export default class App extends Component {
  render( ) {
    let Authenticated = true;
    let page = null;
    if ( Authenticated ) {
      page = <UserPage/>
    } else {
      page = <LandingPage/>
    }

    return ( page );
  };
}
