import React, { Component } from "react";
import { TopNavbar } from "../components/top-navbar";
import GridLayout from "./grid-layout";
import { UserDetail } from "../components/user-details";

export default class UserPage extends Component {
  render( ) {
    return (
      <div>
        <TopNavbar/>
        <div className="grid">
          <UserDetail/>
          <GridLayout/>
        </div>
      </div>
    )
  }
}
